/*

  localStorage
  window.localStorage

*/

// Запись в ЛС
// localStorage.setItem('myCat', 'Tom');
// localStorage.setItem('back', 'green');
// // Чтение с ЛС
// var cat = localStorage.getItem("myCat");
// // Удаление с ЛС
// // localStorage.removeItem("myCat");

// // Если не найдено, вернет Null
// var background = localStorage.getItem("back");
// console.log(background);

// if( background !== null){
//   document.body.style.backgroundColor = background;
// }




// Задание 1:
//   Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
//   После перезагрузки страницы, цвет должен сохранится.

function getRandom(min, max) {

    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;

}

function getRandomAlpha() {

    a = Math.random();
    return a.toFixed(2);

}

function changeBG() {

    r = getRandom(0, 256);
    g = getRandom(0, 256);
    b = getRandom(0, 256);
    a = getRandomAlpha();

    var color = document.body.style.background = "rgba( " + r + ", " + g + ", " + b + ", " + a + " )";
    localStorage.setItem('background', color);
    return color;
}


button.addEventListener('click', changeBG);

var back = localStorage.getItem('background');

if (back !== null) {

    document.body.style.background = back;

}


/*
   Задание:
   Скопировать текст из файла RegExp.txt на сайт https://regexr.com/
   Написать регулярное выражение которое найдет:
     1. Все слова.                                   --           \w+
     2. Все совпадения букв от a до e,               --          [a-eA-E]
     3. Года, например 2004                          --          \d\d\d\d  (или)  \d{4}
     4. Слова обернутые в [квадратныеКавычки]        --          \[(\w+)?\]
     5. Все форматы файлов .jpg                      --          \s\.(\w+){3}
     6. Все email                                    --          \b(\w+)@(\w*\.)\w+\b
     7. Все слова написанные с большой буквы         --          [A-Z]
 */

// Задание 2 (в другом файле):
//   Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
//   Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
//   логина.

//   + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
//   + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
//   он введет только admin@example.com и пароль 12345678